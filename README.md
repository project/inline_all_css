# Inline All CSS

The Inline All CSS module replaces the entire CSS rendering service with an
implementation that renders all assets within inline style tags.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/inline_all_css).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/inline_all_css).


## Table of contents

- Requirements
- Recommended
- Installation
- Configuration
- FAQ
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Recommended

To use this module it is recommended to have a [Content Security Policy]
(https://content-security-policy.com/) configured that does not allow unsafe-inline
in the script-src source value.

For more information about using a Content Security Policy with Drupal,please check
out the [CSP](https://www.drupal.org/project/csp) project.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Configure the module in System » Inline all css:
  - Enable
    This setting will allow the module to begin inlining CSS assets.

  - Enabled themes
    This setting allows for enabling CSS inlining on individual themes.

  - Minify
    This setting allows for minifying the resulting inline CSS.


## FAQ

**Q: Will this make my site faster?**

**A:** This question is impossible to answer without performing a performance evaluation.


## Maintainers

- Luke Leber - [Luke.Leber](https://www.drupal.org/u/lukeleber)
