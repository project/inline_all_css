<?php

/**
 * @file
 * Contains post update functions for Inline All CSS.
 */

/**
 * Ensure that module behavior does not change from the RC2 release.
 */
function inline_all_css_post_update_retain_rc2_state() {
  \Drupal::configFactory()
    ->getEditable('inline_all_css.settings')
    ->set('enabled', TRUE)
    ->save();
}
