<?php

/**
 * @file
 * Sets up the database state for the upgrade path tests.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();

// Set the schema version.
$connection->merge('key_value')
  ->condition('collection', 'system.schema')
  ->condition('name', 'inline_all_css')
  ->fields([
    'collection' => 'system.schema',
    'name' => 'inline_all_css',
    'value' => 's:4:"9000";',
  ])
  ->execute();

// Add the proper core.extension configuration.
$extensions = $connection->select('config')
  ->fields('config', ['data'])
  ->condition('collection', '')
  ->condition('name', 'core.extension')
  ->execute()
  ->fetchField();
$extensions = unserialize($extensions, ['allowed_classes' => []]);
$extensions['module']['inline_all_css'] = 9000;
$connection->update('config')
  ->fields([
    'data' => serialize($extensions),
  ])
  ->condition('collection', '')
  ->condition('name', 'core.extension')
  ->execute();
