<?php

namespace Drupal\inline_all_css_csp_test\EventSubscriber;

use Drupal\inline_all_css\Event\CssPreRenderEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Acts to normalize the output of the collection renderer service.
 */
class CssPreRenderSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CssPreRenderEvent::EVENT_NAME => ['onCssPreRender', 0],
    ];
  }

  /**
   * Normalizes the CSS that is being inlined.
   *
   * @param \Drupal\inline_all_css\Event\CssPreRenderEvent $event
   *   The css pre-render event.
   */
  public function onCssPreRender(CssPreRenderEvent $event) {
    $event->setCss('* { background-color: pink; }');
  }

}
