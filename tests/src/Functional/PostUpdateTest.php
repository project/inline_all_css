<?php

namespace Drupal\Tests\inline_all_css\Functional;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Test cases for the incremental upgrade path.
 *
 * @group inline_all_css
 */
class PostUpdateTest extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->configFactory = $this->container->get('config.factory');
  }

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {

    $fixture = version_compare(\Drupal::VERSION, '10.0.0', '>') ? '9.4.0' : '9.0.0';

    $this->databaseDumpFiles = [
      DRUPAL_ROOT . "/core/modules/system/tests/fixtures/update/drupal-$fixture.bare.standard.php.gz",
      __DIR__ . '/../../fixtures/update/inline_all_css.update-hook-test.php',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function doSelectionTest() {
    parent::doSelectionTest();
    $this->assertSession()->responseContains('Ensure that module behavior does not change from the RC2 release.');
  }

  /**
   * Test case for the upgrade path.
   */
  public function testUpgrade() {
    static::assertArrayNotHasKey('inline_all_css.settings', $this->configFactory->listAll());
    $this->runUpdates();
    static::assertTrue($this->config('inline_all_css.settings')->get('enabled'));
  }

}
