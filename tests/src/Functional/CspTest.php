<?php

namespace Drupal\Tests\inline_all_css\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test cases for the CSP module integration.
 *
 * @group inline_all_css
 */
class CspTest extends BrowserTestBase {

  /**
   * The known sha-256 hash for the string "* { background-color: pink; }".
   */
  protected const KNOWN_HASH = "'sha256-/GsMY1XPRbRQazovTmqsA9WviaNHUWkJVm+XsfN3Hvk='";

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'inline_all_css_csp_test',
    'inline_all_css',
    'csp',
    'csp_extras',
  ];

  /**
   * Test case for the CSP integration.
   */
  public function testCspIntegration() {

    $this->drupalLogin($this->drupalCreateUser([], NULL, TRUE));
    $this->drupalGet(Url::fromRoute('inline_all_css.config'));

    $assert = $this->assertSession();

    // Ensure no hash is added by default.
    $assert->responseHeaderExists('Content-Security-Policy-Report-Only');
    $assert->responseHeaderNotContains('Content-Security-Policy-Report-Only', static::KNOWN_HASH);

    $page = $this->getSession()->getPage();

    $page->findField('Enable')->check();
    $page->findButton('Save configuration')->press();

    // Ensure that the hash is now added.
    $assert->responseHeaderExists('Content-Security-Policy-Report-Only');
    $assert->responseHeaderContains('Content-Security-Policy-Report-Only', static::KNOWN_HASH);

    // Enable unsafe-inline for the script-src-elem directive.
    $this->drupalGet('/admin/config/system/csp');
    $this->submitForm([
      'report-only[directives][style-src][flags][unsafe-inline]' => TRUE,
    ], 'Save configuration');

    // Ensure that the hash is no longer added.
    $this->drupalGet(Url::fromRoute('<front>'));
    $assert->responseHeaderExists('Content-Security-Policy-Report-Only');
    $assert->responseHeaderNotContains('Content-Security-Policy-Report-Only', static::KNOWN_HASH);

  }

}
