<?php

namespace Drupal\Tests\inline_all_css\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test cases for the settings form.
 *
 * @group inline_all_css
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'inline_all_css',
  ];

  /**
   * Test case for the settings form.
   */
  public function testSettingsForm() {
    $session = $this->getSession();

    // Ensure that access is denied by default.
    $this->drupalGet(Url::fromRoute('inline_all_css.config'));
    static::assertEquals(403, $session->getStatusCode());

    // Ensure that access is granted with the proper permission.
    $this->drupalLogin($this->drupalCreateUser(['administer inline all css']));
    $this->drupalGet(Url::fromRoute('inline_all_css.config'));
    static::assertEquals(200, $session->getStatusCode());

    $page = $session->getPage();
    $assert = $this->assertSession();

    // Ensure the proper initial form state.
    $assert->pageTextContains('If there is a content security policy in place, please be sure to test this setting before enabling on production!');
    $enabled = $page->findField('Enable');
    static::assertFalse($enabled->isChecked());

    // Save some settings.
    $enabled->check();
    $page->findField('stark')->check();
    $page->findButton('Save configuration')->press();
    $assert->pageTextContains('The configuration options have been saved.');

    // Ensure that the new form state matches expectations.
    $enabled = $page->findField('Enable');
    $stark = $page->findField('stark');

    static::assertTrue($enabled->isChecked());
    static::assertTrue($stark->isChecked());

    // Ensure that the configuration itself is in the right state.
    $config = \Drupal::config('inline_all_css.settings');
    static::assertTrue($config->get('enabled'));
    static::assertEquals(['stark'], $config->get('enabled_themes'));
  }

}
