<?php

namespace Drupal\Tests\inline_all_css\Unit;

use Drupal\inline_all_css\Event\CssPreRenderEvent;
use Drupal\Tests\UnitTestCase;

/**
 * Test cases for the css pre-render event.
 *
 * @group inline_all_css
 */
class CssPreRenderEventTest extends UnitTestCase {

  /**
   * Test case for the css pre-render event.
   */
  public function testCssPreRenderEvent() {
    $event = new CssPreRenderEvent('* { color: red; }');
    static::assertSame('* { color: red; }', $event->getCss());

    $new_css = 'html { color: green; }';
    $event->setCss($new_css);
    static::assertSame($new_css, $event->getCss());
  }

}
