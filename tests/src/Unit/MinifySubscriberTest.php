<?php

namespace Drupal\Tests\inline_all_css\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\inline_all_css\Event\CssPreRenderEvent;
use Drupal\inline_all_css\EventSubscriber\MinifySubscriber;
use Drupal\Tests\UnitTestCase;

/**
 * Test cases for the minify subscriber.
 *
 * @group inline_all_css
 */
class MinifySubscriberTest extends UnitTestCase {

  /**
   * The expected events.
   */
  protected const EXPECTED_EVENTS = [
    CssPreRenderEvent::EVENT_NAME => ['onCssPreRender', PHP_INT_MIN + 100],
  ];

  /**
   * The non-minified source.
   */
  protected const SOURCE = <<<CSS
* {
  background-color: pink;
}
CSS;

  /**
   * The minified source.
   */
  protected const MINIFIED = <<<CSS
*{background-color:pink;}

CSS;

  /**
   * The subject under test.
   *
   * @var \Drupal\inline_all_css\EventSubscriber\MinifySubscriber
   */
  protected $instance;

  /**
   * The enable flag for minification.
   *
   * @var bool
   */
  protected $minificationEnabled;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $config = $this->getMockBuilder(ImmutableConfig::class)
      ->disableOriginalConstructor()
      ->getMock();

    $config
      ->method('get')
      ->willReturnCallback(function () {
        return $this->minificationEnabled;
      });

    $config_factory = $this->getMockBuilder(ConfigFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $config_factory
      ->method('get')
      ->willReturn($config);

    $this->instance = new MinifySubscriber($config_factory);

  }

  /**
   * Test case for the minify subscriber.
   */
  public function testMinifySubscriber() {

    // Ensure the proper events are subscribed to.
    static::assertSame(static::EXPECTED_EVENTS, MinifySubscriber::getSubscribedEvents());

    $event = new CssPreRenderEvent(static::SOURCE);

    // Ensure minification does not run if disabled.
    $this->minificationEnabled = FALSE;
    $this->instance->onCssPreRender($event);
    static::assertSame(static::SOURCE, $event->getCss());

    // Ensure minification does run if enabled.
    $this->minificationEnabled = TRUE;
    $this->instance->onCssPreRender($event);
    static::assertSame(static::MINIFIED, $event->getCss());
  }

}
