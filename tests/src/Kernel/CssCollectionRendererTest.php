<?php

namespace Drupal\Tests\inline_all_css\Kernel;

use Drupal\Core\Asset\AssetCollectionRendererInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Theme\ActiveTheme;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\inline_all_css\Asset\CriticalCssCollectionRenderer;
use Drupal\inline_all_css\Event\CssPreRenderEvent;
use Drupal\KernelTests\KernelTestBase;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Test cases for the critical css collection renderer.
 *
 * @group inline_all_css
 */
class CssCollectionRendererTest extends KernelTestBase {

  protected const ASSETS = [
    ['type' => 'file', 'data' => 'public://test-1.css'],
    ['type' => 'file', 'data' => 'public://test-2.css'],
    ['type' => 'file', 'data' => 'public://test-3.css'],
    ['type' => 'external', 'data' => 'https://www.example.com/test-4.css'],
  ];

  /**
   * The subject under test.
   *
   * @var \Drupal\inline_all_css\Asset\CriticalCssCollectionRenderer
   */
  protected $instance;

  /**
   * The mocked css collection renderer.
   *
   * @var \Drupal\Core\Asset\AssetCollectionRendererInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $cssCollectionRenderer;

  /**
   * The mocked theme manager service.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $themeManager;

  /**
   * The mock config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The mock event dispatcher.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The mock request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $filesystem = $this->getMockBuilder(FileSystemInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $filesystem
      ->method('realpath')
      ->willReturnMap([
        ['public://test-1.css', __DIR__ . '/../../fixtures/test-1.css'],
        ['public://test-2.css', __DIR__ . '/../../fixtures/test-2.css'],
        ['public://test-3.css', __DIR__ . '/../../fixtures/test-3.css'],
      ]);

    $this->eventDispatcher = $this->getMockBuilder(EventDispatcherInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->cssCollectionRenderer = $this->getMockBuilder(AssetCollectionRendererInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->themeManager = $this->getMockBuilder(ThemeManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $config_factory = $this->getMockBuilder(ConfigFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->config = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->getMock();

    $config_factory
      ->method('get')
      ->willReturn($this->config);

    $stream = $this->getMockBuilder(StreamInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $stream
      ->method('getContents')
      ->willReturn(<<<CSS
div {
  color: green;
}

CSS
    );

    $response = $this->getMockBuilder(ResponseInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $response
      ->method('getBody')
      ->willReturn($stream);

    $http_client = $this->getMockBuilder(ClientInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $http_client
      ->method('request')
      ->willReturn($response);

    $logger = $this->getMockBuilder(LoggerChannelInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $logger_factory = $this->getMockBuilder(LoggerChannelFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $logger_factory
      ->method('get')
      ->willReturn($logger);

    $request_stack = $this->getMockBuilder(RequestStack::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->request = $this->getMockBuilder(Request::class)
      ->disableOriginalConstructor()
      ->getMock();
    $request_stack
      ->method('getCurrentRequest')
      ->willReturn($this->request);

    $this->instance = new CriticalCssCollectionRenderer($this->cssCollectionRenderer, $config_factory, $this->themeManager, $filesystem, $this->eventDispatcher, $http_client, $logger_factory, $request_stack);
  }

  /**
   * Sets the mock module configuration.
   *
   * @param bool $enabled
   *   Should inlining be enabled?
   * @param string[] $enabled_themes
   *   An array of themes to enable inlining for.
   */
  protected function setConfig($enabled, array $enabled_themes) {
    $this->config
      ->method('get')
      ->willReturnMap([
        ['enabled', $enabled],
        ['enabled_themes', $enabled_themes],
      ]);
  }

  /**
   * Sets up the CSS to add in the dispatched event.
   */
  protected function setEventCss() {
    $this->eventDispatcher->method('dispatch')
      ->willReturnCallback(static function (CssPreRenderEvent $event) {
        $css = $event->getCss();
        $css .= <<<CSS
p {
  color: gray;
}
CSS;
        $event->setCss($css);
        return $event;
      });
  }

  /**
   * Sets the current active theme.
   *
   * @param string $theme
   *   The theme to set.
   */
  protected function setActiveTheme($theme) {
    $active_theme = $this->getMockBuilder(ActiveTheme::class)
      ->disableOriginalConstructor()
      ->getMock();

    $active_theme
      ->method('getName')
      ->willReturn($theme);

    $this->themeManager
      ->method('getActiveTheme')
      ->willReturn($active_theme);
  }

  /**
   * Test case for when an XHR request is encountered.
   */
  public function testCssCollectionRendererXhrRequest() {
    $this->setConfig(TRUE, []);

    $this->request
      ->method('isXmlHttpRequest')
      ->willReturn(TRUE);

    // The decorated service should be called.
    $this->cssCollectionRenderer
      ->expects(static::once())
      ->method('render');

    $this->instance->render(static::ASSETS);

  }

  /**
   * Test case for when the enabled flag is not set.
   */
  public function testCssCollectionRendererDisabled() {
    $this->setConfig(FALSE, []);

    // The decorated service should be called.
    $this->cssCollectionRenderer
      ->expects(static::once())
      ->method('render');

    $this->instance->render(static::ASSETS);
  }

  /**
   * Test case for when the enabled flag is set, but the theme is wrong.
   */
  public function testCssCollectionRendererWrongTheme() {
    $this->setConfig(TRUE, ['some_theme']);

    $this->setActiveTheme('a_different_theme');

    // The decorated service should be called.
    $this->cssCollectionRenderer
      ->expects(static::once())
      ->method('render');

    $this->instance->render(static::ASSETS);
  }

  /**
   * Test case for when the enabled flag is set and all themes are enabled.
   */
  public function testCssCollectionRendererAllThemes() {
    $this->setConfig(TRUE, []);

    $this->setActiveTheme('should_not_matter');

    // The decorated service should be called.
    $this->cssCollectionRenderer
      ->expects(static::never())
      ->method('render');

    $this->instance->render(static::ASSETS);
  }

  /**
   * Test case for inline CSS rendering.
   */
  public function testCssCollectionRendererEnabled() {

    $this->setConfig(TRUE, ['my_theme']);
    $this->setActiveTheme('my_theme');

    // The decorated service should not be called.
    $this->cssCollectionRenderer
      ->expects(static::never())
      ->method('render');

    $this->setEventCss();

    $expected = [
      [
        '#type' => 'html_tag',
        '#tag' => 'style',
        '#value' => Markup::create(<<<CSS
* {
  color: red;
}
html {
  color: green;
}
body {
  color: blue;
}
div {
  color: green;
}
p {
  color: gray;
}
CSS),
      ],
    ];
    $actual = $this->instance->render(static::ASSETS);
    static::assertEquals($expected, $actual);
  }

  /**
   * Test case for when there are no assets (some big_pipe responses).
   */
  public function testCssCollectionRendererNoAssets() {
    $this->setConfig(TRUE, []);

    $this->setActiveTheme('should_not_matter');
    $this->cssCollectionRenderer
      ->method('render')
      ->willReturn([]);

    static::assertSame([], $this->instance->render([]));
  }

}
