<?php

namespace Drupal\inline_all_css;

use Drupal\Core\Asset\CssCollectionOptimizer;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * A service provider implementation added for Drupal 10.1+.
 *
 * @todo Figure something else out in https://www.drupal.org/i/3394740
 */
class InlineAllCssServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if (version_compare(\Drupal::VERSION, '10.1', '>=')) {
      if ($container->hasDefinition('asset.css.collection_optimizer')) {
        $definition = $container->getDefinition('asset.css.collection_optimizer');
        $definition->setClass(CssCollectionOptimizer::class);
        $definition->setArguments([
          new Reference('asset.css.collection_grouper'),
          new Reference('asset.css.optimizer'),
          new Reference('asset.css.dumper'),
          new Reference('state'),
          new Reference('file_system'),
        ]);
      }
    }
  }

}
