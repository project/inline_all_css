<?php

namespace Drupal\inline_all_css\EventSubscriber;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\csp\Csp;
use Drupal\csp\Event\PolicyAlterEvent;
use Drupal\inline_all_css\Event\CssPreRenderEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * An event subscriber that minifies the aggregated source.
 */
class CspSubscriber implements EventSubscriberInterface {

  /**
   * The module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $moduleHandler;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Creates an event subscriber instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The config factory service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, EventDispatcherInterface $event_dispatcher) {
    $this->moduleHandler = $module_handler;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      CssPreRenderEvent::EVENT_NAME => ['onCssPreRender', PHP_INT_MIN + 10],
    ];
  }

  /**
   * Optimizes the CSS that is being inlined.
   *
   * @param \Drupal\inline_all_css\Event\CssPreRenderEvent $pre_render_event
   *   The css pre-render event.
   */
  public function onCssPreRender(CssPreRenderEvent $pre_render_event) {
    // If the CSP module exists, add a hash to 'style-src-elem'.
    if ($this->moduleHandler->moduleExists('csp')) {
      $css = $pre_render_event->getCss();
      if (!empty($css)) {
        $listener = function (PolicyAlterEvent $policy_alter_event) use ($css, &$listener) {

          $policy = $policy_alter_event->getPolicy();

          if ($policy->hasDirective('style-src') && !in_array(Csp::POLICY_UNSAFE_INLINE, $policy->getDirective('style-src'), TRUE)) {
            $hash = Csp::calculateHash($css);
            $policy->fallbackAwareAppendIfEnabled('style-src', ["'{$hash}'"]);
          }

          // Remove this particular listener after it's fired.
          $this->eventDispatcher->removeListener('csp.policy_alter', $listener);
        };

        // Set an absurdly low priority in order to safely assume that this
        // listener will be the last one called while still allowing just a tiny
        // bit of leeway for other developers to side-step this assumption.
        $this->eventDispatcher->addListener('csp.policy_alter', $listener, PHP_INT_MIN + 10);
      }
    }
  }

}
