<?php

namespace Drupal\inline_all_css\EventSubscriber;

use Drupal\Core\Asset\CssOptimizer;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\inline_all_css\Event\CssPreRenderEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * An event subscriber that minifies the aggregated source.
 */
class MinifySubscriber extends CssOptimizer implements EventSubscriberInterface {

  /**
   * The module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Creates an event subscriber instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   *
   * @noinspection MagicMethodsValidityInspection
   * @noinspection PhpMissingParentConstructorInspection
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('inline_all_css.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      CssPreRenderEvent::EVENT_NAME => ['onCssPreRender', PHP_INT_MIN + 100],
    ];
  }

  /**
   * Optimizes the CSS that is being inlined.
   *
   * @param \Drupal\inline_all_css\Event\CssPreRenderEvent $event
   *   The css pre-render event.
   */
  public function onCssPreRender(CssPreRenderEvent $event) {
    if ($this->config->get('minify') === TRUE) {
      $css = $event->getCss();
      $css = $this->processCss($css, TRUE);
      $event->setCss($css);
    }
  }

}
