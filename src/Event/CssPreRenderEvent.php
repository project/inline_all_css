<?php

namespace Drupal\inline_all_css\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired before css is rendered.
 */
class CssPreRenderEvent extends Event {

  public const EVENT_NAME = 'inline_all_css_css_pre_render';

  /**
   * The css string that is about to be rendered.
   *
   * @var string
   */
  protected $css;

  /**
   * Creates a new css pre-render event.
   *
   * @param string $css
   *   The initial css of the event.
   */
  public function __construct($css) {
    $this->css = $css;
  }

  /**
   * Gets the css string for this event.
   *
   * @return string
   *   The css string for this event.
   */
  public function getCss() {
    return $this->css;
  }

  /**
   * Sets the css string for this event.
   *
   * @param string $css
   *   The CSS string to set.
   */
  public function setCss($css) {
    $this->css = $css;
  }

}
