<?php

namespace Drupal\inline_all_css\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for the inline all css module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->themeHandler = $container->get('theme_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['inline_all_css.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'inline_all_css_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('inline_all_css.settings');

    $form['warning'] = [
      '#type' => 'markup',
      '#markup' => <<<HTML
<p class="messages messages--warning">{$this->t('If there is a content security policy in place, please be sure to test this setting before enabling on production!')}</p>
HTML,
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#default_value' => $config->get('enabled'),
    ];

    $themes = $this->themeHandler->listInfo();
    foreach ($themes as $k => $theme) {
      $themes[$k] = $theme->getName();
    }

    $form['enabled_themes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled themes'),
      '#description' => $this->t('If no themes are selected, css will be inlined on all themes'),
      '#options' => $themes,
      '#multiple' => TRUE,
      '#default_value' => $config->get('enabled_themes'),
      '#states' => [
        'visible' => [
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['minify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Minify'),
      '#description' => $this->t('If selected, assets will be minified after aggregation'),
      '#default_value' => $config->get('minify'),
      '#states' => [
        'visible' => [
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('inline_all_css.settings');
    $config->set('enabled', $form_state->getValue('enabled'));

    $enabled_themes = $form_state->getValue('enabled_themes');

    // Only take the values.
    $enabled_themes = array_values($enabled_themes);

    // Filter out any empties.
    $enabled_themes = array_filter($enabled_themes);

    $config->set('enabled_themes', $enabled_themes);
    $config->set('minify', $form_state->getValue('minify'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
